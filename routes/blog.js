const express = require('express')
const router = express.Router()
const {
  getList,
  getDetail,
  newBlog,
  updateBlog,
  delBlog,
} = require('../controller/blog')
const loginCheck = require('../middleware/loginCheck')
const { SuccessModel, ErrorModel } = require('../model/resModel')

router.get('/list', function (req, res, next) {
  const author = req.query.author || ''
  const keyword = req.query.keyword || ''

  if (req.query.isadmin) {
    // 管理员页面
    if (req.session.username == null) {
      console.error(`is admin, no login`)
      // 未登录
      res.json(new ErrorModel('未登录'))
      return
    }
    author = req.session.username
  }

  const result = getList(author, keyword)
  return result.then((listData) => res.json(new SuccessModel(listData)))
})

router.get('/detail', function (req, res, next) {
  const id = req.query.id
  const result = getDetail(id)
  return result.then((listData) => res.json(new SuccessModel(listData)))
})

router.post('/new', loginCheck, function (req, res, next) {
  const blogData = req.body
  const result = newBlog(blogData)
  return result.then((data) => res.json(new SuccessModel(data)))
})

router.post('/update', loginCheck, function (req, res, next) {
  const id = req.query.id
  const blogData = req.body
  const result = updateBlog(id, blogData)
  return result.then((val) => {
    if (val) {
      res.json(new SuccessModel())
    } else {
      res.json(new ErrorModel('更新博客失败'))
    }
  })
})

router.post('/del', loginCheck, function (req, res, next) {
  const id = req.query.id
  const author = req.session.username
  const result = delBlog(id, author)
  return result.then((val) => {
    if (val) {
      res.json(new SuccessModel())
    } else {
      res.json(new ErrorModel())
    }
  })
})

module.exports = router
